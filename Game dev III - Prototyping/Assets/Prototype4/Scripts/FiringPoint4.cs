using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Protoype4

{
    public class FiringPoint4 : MonoBehaviour
    {
        public GameObject projectilePrefab;
        public float projectileSpeed = 1000;
        public Transform firingPoint;
        private Projectile gameOverBit;

        // Start is called before the first frame update
        void Start()
        {
           gameOverBit = GameObject.Find("Projectile").GetComponent<Projectile>();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))//&& gameOverBit.gameOver == false) 
            {
                GameObject projectileInstance;
                projectileInstance = Instantiate(projectilePrefab, firingPoint.position, firingPoint.rotation);
                projectileInstance.GetComponent<Rigidbody>().AddForce(firingPoint.forward * projectileSpeed);
                Destroy(projectileInstance, 8);

                    }
        }
    }
}
