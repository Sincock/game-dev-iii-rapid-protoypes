using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Protoype4
{


    public class Projectile : MonoBehaviour
    {
        public bool gameOver = false;

        public void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.CompareTag("Target"))
            {
                collision.collider.GetComponent<Renderer>().material.color = Color.red;

                Destroy(collision.collider.gameObject, 0.1f);

                Destroy(this.gameObject);
            }
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {


            {

            }
        }
    }
}
