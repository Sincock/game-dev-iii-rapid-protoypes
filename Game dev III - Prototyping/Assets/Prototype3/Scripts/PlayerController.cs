using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Protoype3

{

    public class PlayerController : MonoBehaviour
    {
        public float speed = 10;
        public float turnSpeed = 90;
            
        public bool gameOver = false;
        public bool isOnGround = true;
        public GameObject Cylinder1;
        public GameObject Cylinder2;


        // Start is called before the first frame update
        void Start()
        {

        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Floor"))
            {
                isOnGround = true;

            }


            else if (collision.gameObject.CompareTag("Wall"))
            {   
                GetComponent<Renderer>().material.color = Color.black;
                gameOver = true;
                
                Debug.Log("Game Over");
            }

            else if (collision.gameObject.CompareTag("Obstacle"))
            {
                
                GetComponent<Renderer>().material.color = Color.black;
                gameOver = true;
                Debug.Log("Game Over");
            }

            else if (collision.gameObject.CompareTag("Finish Line"))
            { 
                GetComponent<Renderer>().material.color = Color.white;
                gameOver = true;
                Debug.Log("You Win!!!");
            }
        }

        // Update is called once per frame
        void Update()
        {
              if (gameOver == false)
              {

                 transform.position += transform.forward * speed * Time.deltaTime;
              }

             if (Input.GetKey(KeyCode.Mouse0)&& !gameOver)

            {
                
                transform.RotateAround(Cylinder1.transform.position, Vector3.up, -turnSpeed * Time.deltaTime);
            }

           if (Input.GetKey(KeyCode.Mouse1)&& !gameOver) 
             {
                 transform.RotateAround(Cylinder2.transform.position, Vector3.up, turnSpeed * Time.deltaTime);
              }
        }
    }
}
