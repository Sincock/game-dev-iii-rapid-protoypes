using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Protoype3
{
    public class CameraController : MonoBehaviour
    {
        public Transform player;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void LateUpdate()
        {
            transform.position = player.position;
            transform.rotation = player.rotation;   
        }
    }
}
