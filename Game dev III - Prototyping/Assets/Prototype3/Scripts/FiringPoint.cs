using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Protoype3
{
    public class FiringPoint : MonoBehaviour
    {
        public GameObject projectilePrefab;
        public float projectileSpeed = 1000;
        public Transform firingPoint;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space)) 
            {
                GameObject projectileInstance;
                projectileInstance = Instantiate(projectilePrefab, firingPoint.position, firingPoint.rotation);
                projectileInstance.GetComponent<Rigidbody>().AddForce(firingPoint.forward * projectileSpeed);
                Destroy(projectileInstance, 2);

                    }
        }
    }
}
