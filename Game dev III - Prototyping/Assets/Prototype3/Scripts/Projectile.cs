using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Protoype3
{

    public class Projectile : MonoBehaviour
    {
        public void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.CompareTag("Obstacle"))
            {
                collision.collider.GetComponent<Renderer>().material.color = Color.red;

                Destroy(collision.collider.gameObject, 0.1f);
                Debug.Log("Booom!");
                Destroy(this.gameObject);
            }
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}