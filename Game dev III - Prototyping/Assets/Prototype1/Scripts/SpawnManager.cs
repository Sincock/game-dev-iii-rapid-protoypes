using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prototype1
{
    public class SpawnManager : MonoBehaviour
    {
        public List<GameObject> obstaclePrefab = new List<GameObject>();
        private Vector3 spawnPos = new Vector3(30, 0, 0);
        private float startDelay = 2;
        private float repeatRate = 2;
        private PlayerController playerControllerScript;


        // Start is called before the first frame update
        void Start()
        {
            InvokeRepeating("SpawnObstacle", startDelay, repeatRate);
            playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();






        }

        void SpawnObstacle()
        {
            if (playerControllerScript.gameOver == false)
            {
                {
                    int rndObstacle = Random.Range(0, obstaclePrefab.Count);

                    Instantiate(obstaclePrefab[rndObstacle], spawnPos, obstaclePrefab[rndObstacle].transform.rotation);
                }
            }

            // Update is called once per frame
           
        }
    }
}

