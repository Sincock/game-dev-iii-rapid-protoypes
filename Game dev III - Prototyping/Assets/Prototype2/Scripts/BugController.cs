using Prototype1;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
namespace Prototype2




{


    public class BugController : MonoBehaviour

    {
    
        private GameController gameControllerScript;
        public float xAngle, yAngle, zAngle;
        public float Speed = 1;
        
        

        // Start is called before the first frame update
        void Start()
        {   
            InvokeRepeating("BugWalk",0,10);
            gameControllerScript = GameObject.Find("Game Controller").GetComponent<GameController>();
        }

        void BugWalk()
        {
            Sequence mySequence = DOTween.Sequence();

            if (gameControllerScript.gameOver == false)
            {
               mySequence.Append (transform.DOMoveZ(10, 5));
               mySequence.Append (transform.DORotate(new Vector3(180, 0, 0),3));
               mySequence.Append (transform.DOMoveZ(-10,5));
               mySequence.Append(transform.DORotate(new Vector3(180, 0, 0), 3));
                //transform.forward = -transform.forward;


            }
            
        }

        
        // Update is called once per frame
        void Update()
        {
            


            }
        }
    }

