using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
namespace Prototype1
{
    

    public class DieBug : MonoBehaviour
    {   private GameObject Bug;
        
        // Start is called before the first frame update
        void Start()
        {
           Bug = GameObject.Find("Bug").GetComponent<GameObject>();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.CompareTag("Player"))
            {
                Destroy(Bug);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}